open Util

type ident = string

type constant =
  | Bool of bool
  | Num of float
  | String of string

type binop =
  | Binop of string

type exp =
  | This
  | Var of ident
  | Con of constant
  | Assign of exp * exp
  | Field of exp * ident
  | Method of exp * ident * (exp list)
  | Binop of binop * exp * exp
  | Lambda of (ident list) * exp
  | Def of ident * exp
  | Seq of exp * exp (* 命令列 *)
  | Array of exp list (* 配列 *)

type line =
    Line of Date.t * exp

let string_of_const = function
  | Bool b -> !%"B(%b)" b
  | Num f -> !%"N(%f)" f
  | String s -> !%"\"%s\""s

let rec string_of_exp = function
  | This -> "THIS"
  | Var x -> !%"Var(%s)" x
  | Con c -> string_of_const c
  | Method(obj, fname, args) ->
      !%"Method(%s, %s, [%s])" (string_of_exp obj) fname
        (slist";"string_of_exp args)
  | Field(obj, fname) ->
      !%"Field(%s, %s)" (string_of_exp obj) fname
  | _ -> "TODO"

let string_of_line (Line(d, exp)) =
  !%"%f: %s" (Date.unixtime d)(string_of_exp exp)

let to_string lines =
  "[\n" ^ slist "\n" (string_of_line) lines ^ "\n]"

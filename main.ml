open Util
module P = Parser
module PM = ParserMonad

let _ =
  print_endline "start";
  P.parse_file "test.niwango"
  |> Ast.to_string
  |> print_endline


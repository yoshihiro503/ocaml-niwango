open Lazy
open Util

type 'a llist = Nil | Cons of 'a * 'a llist Lazy.t

let hd = function | Nil -> failwith "hd" | Cons (x, xs) -> x
let tl = function | Nil -> failwith "tl" | Cons (x, xs) -> !$xs

let rec map f = function
  | Nil -> Nil
  | Cons (x, xs) -> Cons (f x, lazy (map f !$xs))

let rec (++) xs ys =
  match xs with
  | Nil -> ys
  | Cons(x, xs') -> Cons(x, lazy (!$xs' ++ ys))

let rec concat = function
  | Nil -> Nil
  | Cons(Nil, tail) -> concat !$tail
  | Cons(Cons(x,xs), tail) -> Cons(x, lazy(concat(Cons(!$xs, tail))))

let concat_map f xs = concat (map f xs)

(* llist <--> list *)
let rec of_list = function
  | [] -> Nil
  | x::xs -> Cons(x, lazy (of_list xs))

let rec take n l =
  match n, l with
  | 0, _ -> []
  | n, Nil -> []
  | n, Cons (x, xs) -> x :: take (n-1) !$xs

let rec filter f = function
  | Nil -> Nil
  | Cons(x, lazy xs) when f x ->
      Cons(x, lazy (filter f xs))
  | Cons(x, lazy xs) ->
      filter f xs


(* int llist *)
let rec from n = Cons (n, lazy (from (n+1)))

let rec (--) a b =
  if b - a < 0 then Nil
  else Cons(a, lazy ((a+1) -- b))

(* llist <--> stream *)
let rec of_stream str =
  try
    Cons (Stream.next str, lazy (of_stream str))
  with
  | Stream.Failure -> Nil

let sllist ?(items:int=20) delim show l =
  let fin = take items l in
  if List.length fin <= items then
    slist delim show fin
  else
    slist delim show fin ^ "..."

(* string -> llist *)
let of_string =
  of_stream $ Stream.of_string


(* llist Monad *)

let return x = Cons(x, lazy Nil)
let (>>=) m f = concat_map f m
let guard b = if b then return () else Nil
